
# ---------------------------------
# fish functions
# --------------------------------- 


# ------------------
# common
# ------------------

# go to user Documents dir
function @
  cd ~/Documents/
end


# list all
function l
  ls -alh $argv
end


# tree all
function t
  tree -aL 1 $argv
end


# clear
function c
  clear
end


# pwd then copy dir url
function pwc
  pwd | pbcopy
  echo '==> Path url has been copied to the clipboard'
end


# mkdir then cd into newly created dir
function mch
  mkdir $argv
  cd $argv
end


# ------------------
# dev
# ------------------

# start a new create-react-app project
function creact
  create-react-app $argv
end


# start browser-sync from current dir
function bss
  browser-sync start --server --port=3000 --watch=true --files='./' --ignore='.git/' --directory=true --no-open
end


# start a quick server
function srv
  serve -nlo $argv
end


# edit fish config
function cfg
  #nano ~/.config/fish/config.fish
  # subl ~/.config/fish/config.fish
  code ~/.config/fish/config.fish
end


# ------------------
# git
# ------------------

# git status
function gts
  git status
end


# add all & commit all
function gtc
  git add -A; git commit -m $argv
end


# default push
function gtp
  git push origin master
end


# git log
function gtl
  git log --oneline --graph --decorate
end


# git diff
function gtd
  git diff
end
